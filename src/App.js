import React from 'react';
import Routes from './Routes';
import './sass/app.scss';
import { AppContextProvider } from './components/AppContext';

class App extends React.Component {
  render() {
    return (
      <AppContextProvider>
        <Routes />
      </AppContextProvider>
    );
  }
}

export default App;
