import React, { Component } from 'react';
import appConfig from './config';

const localStorageName = appConfig.LOCAL_STORAGE_NAME;
const getInitialState = () => ({
  user: {
    firstName: '',
    lastName: '',
    email: '',
    accountType: '',
    defaultRoute: '/',
    jwt: '',
  },
  isLoggedIn: false,
});

let localStorageValue = localStorage.getItem(localStorageName);
let localStorageJSONValue = {};

//if no local storage then setting it initially
if (localStorageValue === null) {
  localStorage.setItem(localStorageName, JSON.stringify(getInitialState()));
  localStorageValue = localStorage.getItem(localStorageName);
}
localStorageJSONValue = JSON.parse(localStorageValue || '');

//creating context
const AppContext = React.createContext(localStorageJSONValue);

export const AppContextConsumer = AppContext.Consumer;

class AppContextProvider extends Component {
  constructor(props) {
    super(props);
    this.state = localStorageJSONValue;
  }

  componentDidUpdate = (prevProps, prevState) => {
    const newStateJSON = JSON.stringify(this.state);
    const prevStateJSON = JSON.stringify(prevState);

    if (newStateJSON !== prevStateJSON) {
      localStorage.setItem(localStorageName, newStateJSON);
    }
  };

  login = (data: any) => {
    const newState = {
      user: {
        firstName: data.first_name,
        lastName: data.last_name,
        email: data.email,
        accountType: data.account_type,
        defaultRoute: '/',
        jwt: data.jwt,
      },
      isLoggedIn: true,
    };
    localStorage.setItem(localStorageName, JSON.stringify(newState));

    this.setState(newState);
  };

  logout = () => {
    localStorage.removeItem(localStorageName);

    window.location.href = '/';
  };

  render() {
    const { children } = this.props;
    const value = {
      ...this.state,
      login: this.login,
      logout: this.logout,
    };

    return <AppContext.Provider value={value}>{children}</AppContext.Provider>;
  }
}

export default AppContext;

export { AppContextProvider };
