export default {
  // Add common config values here
  BASE_URL_API: 'http://rewardsplus.in/api/',
  LOCAL_STORAGE_NAME: 'storeAppDataState',
  PRIVACY_POLICY_URL: '',
  TERMS_CONDITIONS_URL: '',
};
