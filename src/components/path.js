export default {
  root: '/',
  slider: '/slider',
  PDP: '/product/:productId',
  search: '/search',
  account: '/account',
  cart: '/cart',
  thankYou: '/thankYou',
};
