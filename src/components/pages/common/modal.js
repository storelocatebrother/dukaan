import React from 'react';
import Modal from 'react-responsive-modal';

const styles = {
  closeButton: {
    cursor: 'pointer',
  },
};

const ModalContainer = (props) => (
  <Modal
    open={props.open}
    onClose={props.onCloseModal}
    center={true}
    styles={styles}
  >
    {React.cloneElement(props.children, { onCloseModal: props.onCloseModal })}
  </Modal>
);

export default ModalContainer;
