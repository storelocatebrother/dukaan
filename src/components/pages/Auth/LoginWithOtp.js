import React from 'react';

class LoginWithOtp extends React.Component {
  render() {
    return (
      <div class="linearForm text-left">
        <div class="title-modal">Login</div>
        <p class="title-desc text-left">
          Enter Mobile No / Email ID to get an OTP for smooth login
        </p>
        <div class="form_field">
          <i class="fas fa-mobile-alt formIcons"></i>
          <input
            type="text"
            class="form-control input-field"
            placeholder="Phone*"
            value=""
            name=""
          />
        </div>
        <div class="form_field">
          <span class="text-right timer">RESENT OTP</span>
          <i class="fas fa-unlock-alt formIcons"></i>
          <input
            type="text"
            class="form-control input-field"
            placeholder="Enter 6 Digit OTP"
            value="Enter 6 Digit OTP"
            name=""
          />
        </div>

        <div class="row">
          <div class="col">
            <i class="fas fa-stopwatch purple-color"></i>{' '}
            <span class="purple-color">00:50</span> Second left
          </div>
          <div class="col">
            <div class="form_field">
              <a href="#" class="btn btn-default field_btn">
                VERIFY OTP
              </a>
            </div>
          </div>
        </div>
        <div class="text-center note-text padding-top-50px">
          By continuing, you agree to our <a href="#">Privacy Policy</a>
          and <a href="#">T&C</a>
        </div>
      </div>
    );
  }
}

export default LoginWithOtp;
