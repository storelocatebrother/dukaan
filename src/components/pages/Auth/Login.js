import React from 'react';
import CloseIcon from '../../../images/close-icon.png';
import TelescopeImg from '../../../images/telescope.png';
import LoginWithOtp from './LoginWithOtp';
import appConfig from '../../config';
import validator from 'validator';

class Login extends React.Component {
  state = {
    showLoginOtpForm: false,
    mobileNo: '',
    password: '',
    errors: {
      mobileNo: '',
      password: '',
    },
  };

  shwOtpFormHandler = () => {
    this.setState({
      showLoginOtpForm: true,
    });
  };

  changeHandler = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  submitHandler = (e) => {
    e.preventDefault();
    const { mobileNo, password } = this.state;
    const errors = {};

    if (!mobileNo) {
      errors.mobileNo = 'Required';
    } else if (
      !validator.isEmail(mobileNo) &&
      !validator.isMobilePhone(mobileNo, 'en-IN')
    ) {
      errors.mobileNo = 'Invalid email or mobile no.';
    }
    if (!password) {
      errors.password = 'Required';
    }

    this.setState({
      errors,
    });

    console.log(this.state.errors);

    return false;
  };

  render() {
    return (
      <div class="modal-get-callback login ">
        <a class="close-modal">
          <img src={CloseIcon} alt="close" />{' '}
        </a>

        <div class="linearGradient text-center">
          <div class="position-top-80">
            <img src={TelescopeImg} alt="image" />
            <h5>India's best lab test platform</h5>
            <p>Get the best lab test services at your doorstep.</p>
            <a href="#" class="field_btn-white">
              Know more
            </a>
          </div>
        </div>
        {!this.state.showLoginOtpForm ? (
          <div class="linearForm text-left">
            <form onSubmit={this.submitHandler}>
              <div class="title-modal">Login</div>
              <p class="title-desc text-left">
                Enter Mobile No / Email ID to get an OTP for smooth login
              </p>
              <div class="form_field">
                <i class="fas fa-mobile-alt formIcons"></i>
                <input
                  type="text"
                  class="form-control input-field"
                  placeholder="Phone*"
                  value={this.state.mobileNo}
                  onChange={this.changeHandler}
                  name="mobileNo"
                />
                {this.state.errors.mobileNo ? (
                  <small class="text-danger">
                    {this.state.errors.mobileNo}
                  </small>
                ) : (
                  ''
                )}
              </div>
              <div class="form_field">
                <i class="fas fa-unlock-alt formIcons"></i>
                <input
                  type="text"
                  class="form-control input-field"
                  placeholder="Enter Password*"
                  value={this.state.password}
                  onChange={this.changeHandler}
                  name="password"
                />
                <small class="text-danger">Required</small>
              </div>
              <div class="form_field text-right">
                <a href="#" class="forgot-password ">
                  Forgot Password ?
                </a>
              </div>

              <div class="row">
                <div class="col">
                  <div class="form_field">
                    <a
                      href="#"
                      class="btn btn-default field_btn-aqua"
                      onClick={this.shwOtpFormHandler}
                    >
                      LOGIN VIA OTP
                    </a>
                  </div>
                </div>
                <div class="col">
                  <div class="form_field">
                    <button class="btn btn-default field_btn">LOGIN</button>
                  </div>
                </div>
              </div>
            </form>
            <div class="text-center note-text padding-top-50px">
              By continuing, you agree to our <a href="#">Privacy Policy</a>
              and <a href="#">T&C</a>
            </div>
          </div>
        ) : (
          <LoginWithOtp />
        )}
      </div>
    );
  }
}

export default Login;
