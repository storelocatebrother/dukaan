import React from 'react';
import Demo from '../../../Demo';
import Header from '../../include/Header';
import Footer from '../../include/Footer';
import axios from 'axios'

class HomePage extends React.Component {

  rewardList = (e) => {
    alert("Gaand mari padi h ");
    e.preventDefault(); 

    const data = {
      "pincode" : "800027"
    }

    axios.post(`https://cors-anywhere.herokuapp.com/http://rewardsplus.in/api/store/storeListing`, data)
    .then((res) => console.log(res.data.payload))
    .catch((err) => console.log(err));
  }
  

  render() {
    return (
      <div>
        <button    onClick={this.rewardList} >Test</button>
                                                                       
        <Header />


        <div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-12 text-center">
            <h2 class="headine2"><span>Featured  </span> Doctors</h2>
            <div class="border_bottom"></div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="featured_box">
                <div class="circle_img"><img src="images/circle-img.png" class="img-responsive center-block" /></div>
                <h4 class="haddng_four">Dr. Kumar Kaushik</h4>
                <p class="doctors_mbbs">DS, DNB, MBBS</p>
                <ul class="star_icon">
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#" class="active"><i class="fa fa-star"></i></a></li>
                    <li>4.5<span>/5</span></li>
                </ul>
                <div class="years">19+ Years Experiences</div>
                <div class="address">Add : 45B, Preet Vihar, New Delhi</div>
                <div class="bg_color1">
                    <div class="row">
                        <div class="col-xs-6 col-md-5 text-center">
                            <a href="#" class="view_btn">VIEW DETAILS</a>
                        </div>
                        <div class="col-xs-6 col-md-7"><a href="#" class="btn btn-default buy_btn1">BOOK APPOINTMENT</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="featured_box">
                <div class="circle_img"><img src="images/circle-img.png" class="img-responsive center-block" /></div>
                <h4 class="haddng_four">Dr. Kumar Kaushik</h4>
                <p class="doctors_mbbs">DS, DNB, MBBS</p>
                <ul class="star_icon">
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#" class="active"><i class="fa fa-star"></i></a></li>
                    <li>4.5<span>/5</span></li>
                </ul>
                <div class="years">19+ Years Experiences</div>
                <div class="address">Add : 45B, Preet Vihar, New Delhi</div>
                <div class="bg_color1">
                    <div class="row">
                        <div class="col-xs-6 col-md-5 text-center">
                            <a href="#" class="view_btn">VIEW DETAILS</a>
                        </div>
                        <div class="col-xs-6 col-md-7"><a href="#" class="btn btn-default buy_btn1">BOOK APPOINTMENT</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div class="featured_box">
                <div class="circle_img"><img src="images/circle-img.png" class="img-responsive center-block" /></div>
                <h4 class="haddng_four">Dr. Kumar Kaushik</h4>
                <p class="doctors_mbbs">DS, DNB, MBBS</p>
                <ul class="star_icon">
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                    <li><a href="#" class="active"><i class="fa fa-star"></i></a></li>
                    <li>4.5<span>/5</span></li>
                </ul>
                <div class="years">19+ Years Experiences</div>
                <div class="address">Add : 45B, Preet Vihar, New Delhi</div>
                <div class="bg_color1">
                    <div class="row">
                        <div class="col-xs-6 col-md-5 text-center">
                            <a href="#" class="view_btn">VIEW DETAILS</a>
                        </div>
                        <div class="col-xs-6 col-md-7"><a href="#" class="btn btn-default buy_btn1">BOOK APPOINTMENT</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-12 text-center">
            <h2 class="headine2"><span>Who   </span> We Are</h2>
            <div class="border_bottom"></div>
        </div>

        <div class="col-md-6 col-xs-12">
            <div class="our_story">OUR STORY</div>
            <div class="we_about"><span>about</span> direct2lab</div>
            <div class="handle"><i class="fa fa-check"></i> Handle With Professionlism</div>
            <div class="praghraf">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mattis sapien eget
                purus dignissim, ac cursus arcu volutpat. Nulla dictum venenatis erat, in rhoncus nulla finibus non.
                Maecenas semper lacinia purus id ultricies. Sed nec ante eget libero tincidunt sodales. Morbi quis diam
                ut diam sagittis molestie sit amet eget leo. Donec eget ligula non risus tristique semper.
            </div>
            <div class="handle"><i class="fa fa-check"></i> We Love What We Do</div>
            <div class="praghraf">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mattis sapien eget
                purus dignissim, ac cursus arcu volutpat. Nulla dictum venenatis erat, in rhoncus nulla finibus non.
                Maecenas semper lacinia purus id ultricies. Sed nec ante eget libero tincidunt sodales. Morbi quis diam
                ut diam sagittis molestie sit amet eget leo. Donec eget ligula non risus tristique semper.
            </div>
            <div class="view_service"><a href="#" class="btn btn-default buy_btn1">VIEW SERVICES</a></div>
        </div>

        <div class="col-md-6 col-xs-12">
            <div class="our_story">SERvices at a glance</div>
            <div class="we_about"><span>why</span> visit us</div>
            <div class="praghraf">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mattis sapien eget
                purus dignissim, ac cursus arcu volutpat. Nulla dictum venenatis erat, in rhoncus nulla finibus non.
                Maecenas semper lacinia purus id ultricies. Sed nec ante eget libero tincidunt sodales. Morbi quis diam
                ut diam sagittis molestie sit amet eget leo. Donec eget ligula non risus tristique semper . Sed nec ante
                eget libero tincidunt sodales.
            </div>
            <div class="praghraf">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mattis sapien eget
                purus dignissim, ac cursus arcu volutpat. Nulla dictum venenatis erat, in rhoncus nulla finibus non.
                Maecenas semper lacinia purus id ultricies. Sed nec ante eget libero tincidunt sodales. Morbi quis diam
                ut diam sagittis molestie sit amet eget leo. Donec eget ligula non risus tristique semper. Sed nec ante
                eget libero tincidunt sodales. Sed nec ante eget libero tincidunt sodales. Sed nec ante eget libero
                tincidunt sodales.
            </div>
            <div class="view_service"><a href="#" class="btn btn-default buy_btn1">KNOW MORE</a></div>
        </div>

    </div>
</div>
        <Footer />
      </div>
    );
  }
}

export default HomePage;
