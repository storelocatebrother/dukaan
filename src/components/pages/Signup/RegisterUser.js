import React from 'react';
import CloseIcon from '../../../images/close-icon.png';
import TelescopeImg from '../../../images/telescope.png';

class RegisterUser extends React.Component {
  render() {
    return (
      <div>
        <div class="modal-get-callback login ">
          <a class="close-modal">
            <img src={CloseIcon} alt="close" />{' '}
          </a>

          <div class="linearGradient text-center">
            <div class="position-top-80">
              <img src={TelescopeImg} alt="image" />
              <h5>India's best lab test platform</h5>
              <p>Get the best lab test services at your doorstep.</p>
              <a href="#" class="field_btn-white">
                Know more
              </a>
            </div>
          </div>
          <div class="linearForm text-left">
            <div class="title-modal">Sign Up</div>
            <p class="title-desc text-left">
              Enter Mobile No / Email ID to get an OTP for smooth login
            </p>
            <div class="form_field">
              <i class="fas fa-mobile-alt formIcons"></i>
              <input
                type="text"
                class="form-control input-field"
                placeholder="Phone*"
                value=""
                name=""
              />
            </div>

            <div class="form_field">
              <a href="#" class="btn btn-default field_btn">
                SIGN UP
              </a>
            </div>
            <div class="text-center note-text">
              By clicking upon signup, you agree to our{' '}
              <a href="#">Privacy Policy</a> and
              <a href="#">T&C</a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RegisterUser;
