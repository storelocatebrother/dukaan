import React, { FunctionComponent, useContext } from 'react';
import { Route, RouteProps, Redirect } from 'react-router-dom';

const AppRoute = ({
  component: Component,
  accessControl = 'any',
  userTypeAccessControl = '',
  ...rest
}) => {
  return <Route {...rest} render={(props) => <Component {...props} />} />;
};

export default AppRoute;
