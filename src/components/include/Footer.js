import React, { Fragment } from 'react';

const Footer = () => (
  <Fragment>
    <div className="">
      <footer className="HJlsB9">
        <div className="TW4owM">
          <div>
            <div className="_2NHqR1 row">
              <div className="_3qd5C5">
                <div className="_18wTSA">ABOUT</div>
                <a href="" className="_287FRX">
                  Contact Us
                </a>
                <a href="" className="_287FRX">
                  About Us
                </a>
                {/* <a href="" className="_287FRX">Careers</a> */}
                {/* <a href="" className="_287FRX">Flipkart Stories</a> */}
                {/* <a href="" className="_287FRX">Press</a> */}
              </div>
              <div className="_3qd5C5">
                <div className="_18wTSA">HELP</div>
                <a href="" className="_287FRX">
                  Payments
                </a>
                <a href="" className="_287FRX">
                  Shipping
                </a>
                <a href="" className="_287FRX">
                  Cancellation &amp; Returns
                </a>
                <a href="" className="_287FRX">
                  FAQ
                </a>
                <a href="" className="_287FRX">
                  Report Infringement
                </a>
              </div>
              <div className="_3qd5C5">
                <div className="_18wTSA">POLICY</div>
                <a href="" className="_287FRX">
                  Return Policy
                </a>
                <a href="" className="_287FRX">
                  Terms Of Use
                </a>
                <a href="" className="_287FRX">
                  Security
                </a>
                <a href="" className="_287FRX">
                  Privacy
                </a>
                {/* <a href="" className="_287FRX">Sitemap</a> */}
                {/* <a href="" className="_287FRX">EPR Compliance</a> */}
              </div>
              <div className="_3qd5C5">
                <div className="_18wTSA">SOCIAL</div>
                <a href="" className="_287FRX">
                  Facebook
                </a>
                <a href="" className="_287FRX">
                  Twitter
                </a>
                <a href="" className="_287FRX">
                  YouTube
                </a>
                {/* <a href="" className="_287FRX">Google Plus</a> */}
              </div>
              <div className="_3qd5C5 ">
                <div className="_38DIp6">
                  <div className="_18wTSA">
                    <span>Mail Us:</span>
                  </div>
                  <div className="_2V3TtE">
                    <div className="_3Lv0nZ">
                      <div className="_3aS5mM _2V3TtE">
                        <p>Address A-26D, LGF Golf Course, </p>
                        <p> Extension Road, Sushant Lok-3 Sector , </p>
                        <p> 57 Gurugram, HR 122003 Hours, </p>
                        <p> Monday—Friday: 9:00AM–5:00PM , </p>
                        <p> Saturday & Sunday: 11:00AM–3:00PM, </p>
                        <p> New Delhi, India.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <div className="middle_header footer_bottom">
      <div className="container">
        <div className="row">
          <div className="col-md-4">
            <ul className="social_icon">
              {/*<li><a><i className="fa fa-facebook" /></a></li>*/}
              {/*<li><a className="active"><i className="fa fa-twitter" /></a></li>*/}
              {/*<li><a><i className="fa fa-wifi" /></a></li>*/}
              {/*<li><a><i className="fa fa-google-plus" /></a></li>*/}
              {/*<li><a><i className="fa fa-linkedin" /></a></li>*/}
              {/*<li><a><i className="fa fa-skype" /></a></li>*/}
              {/*<li><a><i className="fa fa-vimeo" /></a></li>*/}
              {/*<li><a><i className="fa fa-text-width" /></a></li>*/}
            </ul>
          </div>
          <div className="col-md-4 col-xs-12">
            <p className="text-center copy_right">
              Direct2lab All Rights Reserved
            </p>
          </div>
        </div>
      </div>
    </div>
  </Fragment>
);

export default Footer;
