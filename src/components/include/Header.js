import React from 'react';
import PhoneImg from '../../images/phone_icon.png';
import PhotoIcon from '../../images/photo_icon.png';
import Logo from '../../images/logo.png';
import ModalContainer from '../pages/common/modal';
import Login from '../pages/Auth/Login';
import RegisterUser from '../pages/Signup/RegisterUser';

class Header extends React.Component {
  state = {
    openLogin: false,
    openSignup: false,
  };

  onOpenLoginModal = () => {
    this.setState({
      openLogin: true,
    });
  };

  onCloseLoginModal = () => {
    this.setState({
      openLogin: false,
    });
  };

  onOpenSignupModal = () => {
    this.setState({
      openSignup: true,
    });
  };

  onCloseSignupModal = () => {
    this.setState({
      openSignup: false,
    });
  };

  render() {
    return (
      <header>
        <div class="top_stick">
          <div class="container">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <ul class="stick_menu">
                  <li>
                    <i class="fa mail_icon" aria-hidden="true">
                      <img src={PhoneImg} alt="" />{' '}
                    </i>
                    Call us{' '}
                    <a style={{ fontSize: '13px' }} href="tel:011- 23546413">
                      011- 23546413
                    </a>
                  </li>
                  <li>
                    <i class="fa fa-envelope mail_icon" aria-hidden="true"></i>{' '}
                    Email us{' '}
                    <a href="mailto:info@direct2lab.com">info@direct2lab.com</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="middle_header">
          <div class="container">
            <div class="row">
              <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="logo">
                  <a href="#">
                    <img
                      src={Logo}
                      class="img-responsive"
                      alt="Direct To Lab"
                    />
                  </a>
                </div>
              </div>

              <div class="col-md-8 col-sm-8 col-xs-12 pull-right">
                <ul class="middle_right_section">
                  <li>
                    <a href="#" class="btn btn-success btn_download">
                      Download Report
                    </a>
                  </li>
                  <li>
                    <div class="login">
                      <div class="login_email text-white float-left">
                        <a className="pointer" onClick={this.onOpenLoginModal}>
                          Login
                        </a>
                        <a>&nbsp;/&nbsp;</a>
                        <a className="pointer" onClick={this.onOpenSignupModal}>
                          Sign Up
                        </a>
                      </div>
                      <div
                        class="float-right"
                        style={{ background: 'rgb(41, 54, 66)' }}
                      >
                        <span
                          class="fa fa-user-o"
                          style={{ fontSize: '17px' }}
                        ></span>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="menu_bar">
          <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light">
              <button
                class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNavDropdown"
                aria-controls="navbarNavDropdown"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav">
                  <li class="nav-item active">
                    <a class="nav-link" href="#">
                      LAB TESTS
                    </a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
        <ModalContainer
          open={this.state.openLogin}
          onCloseModal={this.onCloseLoginModal}
        >
          <Login />
        </ModalContainer>

        <ModalContainer
          open={this.state.openSignup}
          onCloseModal={this.onCloseSignupModal}
        >
          <RegisterUser />
        </ModalContainer>
      </header>
    );
  }
}

export default Header;
