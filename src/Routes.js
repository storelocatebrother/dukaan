import React from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';
import AppRoute from './components/AppRoute';
import Login from './components/pages/Auth/Login';
import NotFound from './components/pages/Auth/NotFound';
import HomePage from './components/pages/Home/HomePage';

class Routes extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <AppRoute path="/" component={HomePage} exact />
          <AppRoute path="/login" component={Login} exact />
          <AppRoute component={NotFound} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default Routes;
